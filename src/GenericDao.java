import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class GenericDao {
	private static Connection con;

	public Connection getConnection() { 

	try { 
	Class.forName("com.mysql.jdbc.Driver");
	con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/saycar_planilhas","ingrid","123456");
	System.out.println("Conexao ok");
	} catch (ClassNotFoundException e) {
	e.printStackTrace();
	} catch (SQLException e) {
	e.printStackTrace();
	} catch (Exception e) { 
	e.printStackTrace();
	}
	return con; 
	}
	
	public Connection getConnectionAccess() { 

		try { 
		Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		con = DriverManager.getConnection("jdbc:odbc:saycar_relatorios","","");
		System.out.println("Conexao ok");
		} catch (ClassNotFoundException e) {
		e.printStackTrace();
		} catch (SQLException e) {
		e.printStackTrace();
		} catch (Exception e) { 
		e.printStackTrace();
		}
		return con; 
		}



	public static void fechaConexao(){
	try {
		if(con!=null) con.close();
			con =null;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static  void main(String args[]) {
		GenericDao gdao=new GenericDao();
		gdao.getConnectionAccess();
	}
	
	
	
	}


	