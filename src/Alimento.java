import java.util.Date;

public class Alimento {
	
	private String alimento;
	private String quantidade;
	private String unidade_medida;
	
	
	public String getUnidade_medida() {
		return unidade_medida;
	}
	public void setUnidade_medida(String unidade_medida) {
		this.unidade_medida = unidade_medida;
	}
	
	public String getAlimento() {
		return alimento;
	}
	public void setAlimento(String alimento) {
		this.alimento = alimento;
	}
	public String getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}
	
	
	

}
