import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.PrimitiveIterator;

import javax.swing.JOptionPane;

public class DaoIngesta {
	public Connection c;
	public GenericDao gDao;
	
	
	public DaoIngesta() {
		gDao=new GenericDao();
		c=gDao.getConnection();
		
		
	}
	
	public static void main(String args[]) {
		
		
		cabe�alho();
		//publicaListaLabels();
				
		
		DaoIngesta daoIngesta = new DaoIngesta();
		List<String> listaTodosOsAlimentos=daoIngesta.consultaListaTodosOsAlimentos();
		List<Aluno> listaTodosOsAlunos=daoIngesta.consultaListaIDs();
		String textoAlimentos="";
		
		
		for(Aluno aluno:listaTodosOsAlunos) {
								
				List<String> datasAluno= daoIngesta.consultaListaDatasPorID(aluno.getId());
			
			for(int i=0;i<datasAluno.size();i++ ) {
				
				if(textoAlimentos.equals("")) {
					textoAlimentos=";"+ datasAluno.get(i).replace("00:00:00.0","");
				}
				else {
					textoAlimentos=textoAlimentos+";"+ datasAluno.get(i).replace("00:00:00.0","");
				}
							
				
				List<Alimento> alimentosAluno=daoIngesta.consultaListaAlimentosPorID_Data(aluno.getId(), datasAluno.get(i));
				
								
				for(String alimentos:listaTodosOsAlimentos) {
					
					Alimento alimentoAluno=daoIngesta.buscaAlimentos(alimentos,alimentosAluno);
						
							if(alimentoAluno!=null) {
								
								textoAlimentos =textoAlimentos +";"+alimentoAluno.getQuantidade();
							}
							else {
								textoAlimentos =textoAlimentos +";"+0;
							}
						
						
						
				}
				/*DaoIngesta.fileWriter(aluno.getId()+textoAlimentos, "data"+i);
				System.out.println(aluno.getId()+textoAlimentos + " data"+i);
				textoAlimentos="";*/
			}
			DaoIngesta.fileWriter(aluno.getId().replace(" ",";")+textoAlimentos, "data");
			System.out.println(aluno.getId().replace(" ",";")+textoAlimentos + " data");
			textoAlimentos="";
			
			
		}
		
}
	
	public Alimento buscaAlimentos(String alimento,List<Alimento>alimentosAluno) {
	Alimento ali=null;
	Integer quantidade=0;
	String unidadeMedida="";
	
		
		for (Alimento a:alimentosAluno) {
			
			if(alimento.equals(a.getAlimento())) {
				
				quantidade+=Integer.parseInt(a.getQuantidade());
				unidadeMedida=a.getUnidade_medida();
			}
		}
		
		if(quantidade!=0) {
			ali=new Alimento();
			ali.setQuantidade(quantidade.toString());
			ali.setUnidade_medida(unidadeMedida);
			
		}
		
		return ali;
	}
	
	public static void publicaListaLabels() {
		DaoIngesta daoIngesta = new DaoIngesta();
		List<Label> labelsLista=daoIngesta.consultaListaLabels();
		String textoAlimentos="";
		
		for(Label alimentos:labelsLista) {
			textoAlimentos=textoAlimentos+alimentos.getAlimento()+";"+alimentos.getNomeLabel()+";"+alimentos.getDescri��o();
			fileWriter(textoAlimentos, "labels1");
			textoAlimentos="";
		}
		
		
	}
	
	
	
	
	
	
	public static void cabe�alho() {
		DaoIngesta daoIngesta = new DaoIngesta();
		List<String> alimentosLista=daoIngesta.consultaListaTodosOsAlimentos();
		List<Label> labelsLista=daoIngesta.consultaListaLabels();
		String textoAlimentos="";
		String labelAlimentos="";
		
		for(String alimentos:alimentosLista) {
			textoAlimentos=textoAlimentos+";"+alimentos;
			labelAlimentos=labelAlimentos+";"+daoIngesta.cabe�alho2(labelsLista,alimentos.trim());
		}
		
		DaoIngesta.fileWriter("AlunoID;Sigla;Dia1"+labelAlimentos+";Dia2"+labelAlimentos+";Dia3"+labelAlimentos, "data");
		DaoIngesta.fileWriter("AlunoID;Sigla;Dia1"+textoAlimentos+";Dia2"+textoAlimentos+";Dia3"+textoAlimentos, "data");
		/*DaoIngesta.fileWriter("AlunoID;Dia1"+textoAlimentos, "data0");
		DaoIngesta.fileWriter("AlunoID;Dia2"+textoAlimentos, "data1");
		DaoIngesta.fileWriter("AlunoID;Dia3"+textoAlimentos, "data2");*/
		
		
	}
	
	public String cabe�alho2(List<Label> listaLabel,String alimentoProcurado) {
		String nomeLabel="";
		
		for(Label label:listaLabel) {
			if(alimentoProcurado.equals(label.getAlimento().trim())){
				nomeLabel=label.getNomeLabel();
				break;
			}
		}
		
		return nomeLabel;
	}
	
	
	
	public List<Aluno> consultaListaIDs(){
		
		List<Aluno> listaAluno=new ArrayList<Aluno>();
		StringBuffer sql=new StringBuffer();
		
		sql.append("select distinct saycar_planilhas.ingesta.ID from saycar_planilhas.ingesta order by saycar_planilhas.ingesta.ID asc");
		
		
		 try {
			PreparedStatement ps=c.prepareStatement(sql.toString());
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				
				Aluno AlunoConsultado=new Aluno();
				AlunoConsultado.setId(rs.getString("ID"));
				listaAluno.add(AlunoConsultado);
			}
			
			rs.close();
			ps.close();
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaAluno;
		 
		
		
	}
	
	
public List<String> consultaListaDatasPorID(String id){
		
		List<String> listaData=new ArrayList<String>();
		StringBuffer sql=new StringBuffer();
		
		sql.append("select distinct saycar_planilhas.ingesta.Fecha from saycar_planilhas.ingesta where saycar_planilhas.ingesta.ID = ? order by saycar_planilhas.ingesta.Fecha asc");
		
		
		 try {
			PreparedStatement ps=c.prepareStatement(sql.toString());
			ps.setString(1, id);
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				
				String dataConsultada=new String(rs.getString("Fecha"));
				listaData.add(dataConsultada);
			}
			
			rs.close();
			ps.close();
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaData;
		 
		
		
	}

public List<String> consultaListaTodosOsAlimentos(){
	
	List<String> listaAlimentos=new ArrayList<String>();
	StringBuffer sql=new StringBuffer();
	
	sql.append("select distinct saycar_planilhas.ingesta.Alimentos from saycar_planilhas.ingesta order by saycar_planilhas.ingesta.Alimentos asc");
	
	
	 try {
		PreparedStatement ps=c.prepareStatement(sql.toString());
		ResultSet rs=ps.executeQuery();
		
		while(rs.next()){
			
			String dataConsultada=new String(rs.getString("Alimentos"));
			listaAlimentos.add(dataConsultada);
		}
		
		rs.close();
		ps.close();
		
		
	} catch (SQLException e) {
		JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
	}
		
	 return listaAlimentos;
	 
	
	
}


public List<Alimento> consultaListaAlimentosPorID_Data(String id , String data ){
	
	List<Alimento> listaAlimentos=new ArrayList<Alimento>();
	StringBuffer sql=new StringBuffer();
	
	sql.append("select saycar_planilhas.ingesta.Alimentos , saycar_planilhas.ingesta.Cantidad from saycar_planilhas.ingesta where saycar_planilhas.ingesta.ID = ? and saycar_planilhas.ingesta.Fecha = ? order by saycar_planilhas.ingesta.Alimentos asc");
	
	
	 try {
		PreparedStatement ps=c.prepareStatement(sql.toString());
		ps.setString(1, id);
		ps.setString(2, data);
		ResultSet rs=ps.executeQuery();
		
		while(rs.next()){
			Alimento a = new Alimento();
			a.setAlimento(rs.getString("Alimentos"));
			a.setQuantidade(rs.getString("Cantidad"));
			/*a.setUnidade_medida(rs.getString("Unidad"));*/
			listaAlimentos.add(a);
		}
		
		rs.close();
		ps.close();
		
		
	} catch (SQLException e) {
		JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
	}
		
	 return listaAlimentos;
}

public List<Label> consultaListaLabels(){
	
	List<Label> listaLabels=new ArrayList<Label>();
	StringBuffer sql=new StringBuffer();
	
	sql.append("select saycar_planilhas.alimentos.Alimentos,saycar_planilhas.alimentos.Label,saycar_planilhas.alimentos.Descricao from saycar_planilhas.alimentos order by saycar_planilhas.alimentos.Alimentos asc");
	
	
	 try {
		PreparedStatement ps=c.prepareStatement(sql.toString());
		ResultSet rs=ps.executeQuery();
		
		while(rs.next()){
			
			Label labelConsultada=new Label();
			labelConsultada.setAlimento(rs.getString("Alimentos"));
			labelConsultada.setNomeLabel(rs.getString("Label"));
			labelConsultada.setDescri��o(rs.getString("Descricao"));
			listaLabels.add(labelConsultada);
		}
		
		rs.close();
		ps.close();
		
		
	} catch (SQLException e) {
		JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
	}
		
	 return listaLabels;
	 
	
	
}


	public static void fileWriter(String texto, String data) {
		try {
			
			FileWriter writer = new FileWriter("I://saycare//6.Dados_2017//dieta_2017/"+data+".csv",true);
			writer.append(texto +"\n");
		    writer.flush();
	        
	        
	        writer.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	
	
	public static void primeiraForma() {
		DaoIngesta daoIngesta = new DaoIngesta();
		
		
		for(Aluno aluno:daoIngesta.consultaListaIDs()) {
								
				List<String> datas= daoIngesta.consultaListaDatasPorID(aluno.getId());
			
			for(int i=0;i<datas.size();i++ ) {
				
				
				String textoAlimentos=";"+ datas.get(i).replace("00:00:00.0","");
				List<Alimento> alimentos=daoIngesta.consultaListaAlimentosPorID_Data(aluno.getId(), datas.get(i));
				
				
					for (Alimento a:alimentos) {
												
						textoAlimentos =textoAlimentos +";"+a.getAlimento()+";" +a.getQuantidade();
													
					}
					DaoIngesta.fileWriter(aluno.getId()+textoAlimentos, "data"+i);
					System.out.println(aluno.getId()+textoAlimentos + " data "+i);
					textoAlimentos="";
			}	
			
			
	       
		}
		
			
	}



}


	
	


